<?php

use yii\db\Migration;

class m160714_051642_achievement extends Migration {

  public function up() {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable('{{%achievement}}', [
      'id' => $this->primaryKey(),
      'profile_id' => $this->integer(),
      'course_id' => $this->integer(),
      'score' => $this->string(),
      'comment' => $this->string(),
      'status' => $this->smallInteger()->defaultValue(1),
      'created_at' => $this->timestamp(),
      'created_by' => $this->integer(),
      'updated_at' => $this->timestamp(),
      'updated_by' => $this->integer(),
      ], $tableOptions);

    $this->addForeignKey('achievement_profile_id_fk', '{{%achievement}}', 'profile_id', '{{%profile}}', 'id');
    $this->addForeignKey('achievement_course_id_fk', '{{%achievement}}', 'course_id', '{{%course_list}}', 'id');
  }

  public function down() {
    $this->dropForeignKey('achievement_profile_id_fk', '{{%achievement}}');
    $this->dropForeignKey('achievement_course_id_fk', '{{%achievement}}');
    $this->dropTable('{{%achievement}}');
  }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
    }
