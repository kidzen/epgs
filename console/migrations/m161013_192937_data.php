<?php

use yii\db\Migration;

class m161013_192937_data extends Migration {

    public function up() {
        // Add super administrator
        echo "Registering SuperAdmin.... \n";
        $this->RegisterAdmin();
        echo "Done registering SuperAdmin. \n\n";

        echo "Registering student and instructor.... \n";
        $this->RegisterOthers();
        echo "Done registering Student and Instructor. \n\n";

        echo "Add courses.... \n";
        $this->AddCourse();
        echo "Done add course. \n\n";

        echo "Add question list.... \n";
        $this->AddQuestions();
        echo "Done add question list. \n\n";
       
        echo "Add rank list.... \n";
        $this->AddRank();
        echo "Done add rank list. \n\n";

        echo "You may get into the system as SUPERADMIN using these pass : \n";
        echo " \t Username : admin2 \n\t Password : admin2 \n\n";

        echo "You may get into the system as INSTRUCTOR using these pass : \n";
        echo " \t Username : instructor1 \n\t Password : admin2 \n\n";

        echo "You may get into the system as STUDENT using these pass : \n";
        echo " \t Username : student1 \n\t Password : admin2 \n\n";
    }

    public function down() {
        
    }

    private function RegisterAdmin() {

//        $date = new DateTime();
//        $date->setTimestamp(time());
//        $time = $date->format('Y-m-d H:i:s');

        $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
        $auth_key = Yii::$app->security->generateRandomString();
        $this->insert('{{%profile}}', [
            'id' => 1,
            'firstname' => 'adminfirst',
            'lastname' => 'adminlast',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,]);
        $this->insert('{{%user}}', [
            'profile_id' => 1,
            'username' => 'admin2',
            'auth_key' => $auth_key,
            'password_hash' => $password_hash,
            'email' => 'admin2@mail.com',
            'role' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,]);
    }

    private function RegisterOthers() {

        $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
        $auth_key = Yii::$app->security->generateRandomString();
        $this->insert('{{%profile}}', [
            'id' => 2,
            'firstname' => 'instructor1first',
            'lastname' => 'instructor1last',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%user}}', [
            'profile_id' => 2,
            'username' => 'instructor1',
            'auth_key' => $auth_key,
            'password_hash' => $password_hash,
            'email' => 'instructor1@mail.com',
            'role' => 2,
            'status' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
        $auth_key = Yii::$app->security->generateRandomString();
        $this->insert('{{%profile}}', [
            'id' => 3,
            'firstname' => 'student1first',
            'lastname' => 'student1last',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%user}}', [
            'profile_id' => 3,
            'username' => 'student1',
            'auth_key' => $auth_key,
            'password_hash' => $password_hash,
            'email' => 'student1@mail.com',
            'role' => 3,
            'status' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    private function AddCourse() {

        $this->insert('{{%course_list}}', [
            'course_name' => 'General Flying',
            'level' => '1',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    private function AddQuestions() {

        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'START UP / SHUTDOWN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'TAXI',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'TAKE OFF',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'CLIMB / DEPARTURE',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'STRAIGHT & LEVEL',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'STALL',
            'subquestion' => 'CLEAN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'STALL',
            'subquestion' => 'APPROACH',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'STALL',
            'subquestion' => 'BASE TURN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'TURNS',
            'subquestion' => 'MEDIUM',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'TURNS',
            'subquestion' => 'STEEP',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'TURNS',
            'subquestion' => 'MAX RATE',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'BASIC AEROS / SEQUENCE',
            'subquestion' => 'LOOP',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'BASIC AEROS / SEQUENCE',
            'subquestion' => 'STALL TURN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'BASIC AEROS / SEQUENCE',
            'subquestion' => 'SLOW ROLL',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'BASIC AEROS / SEQUENCE',
            'subquestion' => 'ROLL OF TOP',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'ADVANCE AEROS / SEQUENCE',
            'subquestion' => ' ',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'SPIN',
            'subquestion' => 'INCIPIENT',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'SPIN',
            'subquestion' => 'FULLY DEVELOPED',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'LOW FLYING',
            'subquestion' => 'STEP TURN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'LOW FLYING',
            'subquestion' => 'BAD WX CONFIG',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'EMERGENCY CLIMB',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'DECENT / REJOIN',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'APPROACH & LANDING',
            'subquestion' => 'NORMAL',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'APPROACH & LANDING',
            'subquestion' => 'GLIDE',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'APPROACH & LANDING',
            'subquestion' => 'FLAPLESS',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'APPROACH & LANDING',
            'subquestion' => 'LOW LEVEL',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'ATP PROCEDURE',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'AIRMANSHIP',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'EMERGENCIES',
            'subquestion' => 'EFATO',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'EMERGENCIES',
            'subquestion' => 'FLWOP / EFALL',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('{{%question_bank}}', [
            'course_id' => 1,
            'question' => 'EMERGENCIES',
            'subquestion' => 'OTHERS',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    private function AddRank() {
        $this->insert('{{%rank}}', [
            'rank' => 'Leftenan Muda',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

}
