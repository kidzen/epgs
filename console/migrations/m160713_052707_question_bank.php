<?php

use yii\db\Migration;

class m160713_052707_question_bank extends Migration {

  public function up() {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable('{{%question_bank}}', [
      'id' => $this->primaryKey(),
      'course_id' => $this->integer(),
      'question' => $this->string()->notNull(),
      'subquestion' => $this->string(),
//            'hint' => $this->string(),
//            'type' => $this->integer(),
      'status' => $this->smallInteger()->defaultValue(1),
      'created_at' => $this->timestamp(),
      'created_by' => $this->integer(),
      'updated_at' => $this->timestamp(),
      'updated_by' => $this->integer(),
      ], $tableOptions);

    $this->addForeignKey('question_bank_course_id_fk', '{{%question_bank}}', 'course_id', '{{%course_list}}', 'id');
  }

  public function down() {
    $this->dropForeignKey('question_bank_course_id_fk', '{{%question_bank}}');
    $this->dropTable('{{%question_bank}}');
  }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
    }
