<?php

use yii\db\Migration;

class m160712_193542_profile extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rank}}', [
            'id' => $this->primaryKey(),
            'rank' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'rank_id' => $this->integer(),
            'gender' => $this->string(),
            'race' => $this->string(),
            'address' => $this->string(),
            'state' => $this->string(),
            'poscode' => $this->integer(),
            'country' => $this->string(),
            'unit' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('fk_user_profile_id', '{{%user}}', 'profile_id', '{{%profile}}', 'id');
        $this->addForeignKey('fk_profile_rank_id', '{{%profile}}', 'rank_id', '{{%rank}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_user_profile_id', '{{%user}}');
        $this->dropForeignKey('fk_profile_rank_id', '{{%profile}}');
        $this->dropTable('{{%rank}}');
        $this->dropTable('{{%profile}}');
    }

}
