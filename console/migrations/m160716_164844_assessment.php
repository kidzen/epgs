<?php

use yii\db\Migration;

class m160716_164844_assessment extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%assessment}}', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer(),
//            'profile_id' => $this->integer(),
//            'instructor_id' => $this->integer(),
            'user_course_list_id' => $this->integer(),
            'instructor_id' => $this->integer(),
            'profile_grade' => $this->integer(),
//            'flag_answered' => $this->integer(),
            'type' => $this->integer(),
            'comments' => $this->text(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('assessment_question_id_fk', '{{%assessment}}', 'question_id', 'question_bank', 'id');
        $this->addForeignKey('user_course_list_id_fk', '{{%assessment}}', 'user_course_list_id', '{{%user_course_list}}', 'id');
//        $this->addForeignKey('assessment_profile_id_fk', '{{%assessment}}', 'profile_id', '{{%profile}}', 'id');
//        $this->addForeignKey('assessment_instructor_id_fk', 'assessment', 'instructor_id', '{{%profile}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('assessment_question_id_fk', '{{%assessment}}');
        $this->dropForeignKey('user_course_list_id_fk', '{{%assessment}}');
//        $this->dropForeignKey('assessment_profile_id_fk', '{{%assessment}}');
//        $this->dropForeignKey('assessment_instructor_id_fk', '{{%assessment}}');
        $this->dropTable('{{%assessment}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
