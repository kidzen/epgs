<?php

use yii\db\Migration;

class m160712_201638_user_course_list extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_course_list}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'instructor_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('fk_user_course_list_course_list', '{{%user_course_list}}', 'course_id', '{{%course_list}}', 'id');
        $this->addForeignKey('fk_user_course_list_profile_id', '{{%user_course_list}}', 'profile_id', '{{%profile}}', 'id');
        $this->addForeignKey('fk_user_course_list_instructor_id', '{{%user_course_list}}', 'instructor_id', '{{%profile}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_user_course_list_course_list', '{{%user_course_list}}');
        $this->dropForeignKey('fk_user_course_list_profile_id', '{{%user_course_list}}');
        $this->dropForeignKey('fk_user_course_list_instructor_id', '{{%user_course_list}}');
        $this->dropTable('{{%user_course_list}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
