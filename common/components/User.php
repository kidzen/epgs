<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

class User extends \yii\web\User {

    public function getStatus() {
        $identity = $this->getIdentity();

        return $identity !== null ? 'Online' : 'Offline';
    }
    public function getProfile() {
        $identity = $this->getIdentity();

        return $identity !== null ?  $identity->getProfileId() : null;
    }
    public function getProfileName() {
        $identity = $this->getIdentity();

        return $identity !== null ?  $identity->getProfileName() : null;
    }

    public function getName() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getName() : 'Guest';
    }

    public function getJoinDate() {
        $identity = $this->getIdentity();

        return $identity !== null ? date('M. Y', strtotime($identity->getJoinDate())) : null;
    }

    public function getRole() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getRole() : null;
    }

    public function getRoleName() {
        $identity = $this->getRole();
//        var_dump($this->getRole());
//        echo 'matai';
//        die();
        if ($identity !== null) {
            if ($identity == 1) {
                return 'Administrator';
            } else if ($identity == 2) {
                return 'Instructor';
            } else if ($identity == 3) {
                return 'Student';
//            } else if ($identity == 4){
//                return 'Manager';
//            } else if ($identity == 5){
//                return 'Developer';
//            } else if ($identity == 0){
//                return 'Unregistered';
            } else {
                return 'Unregistered';
            }
        } else {
            return 'Guest';
        }
    }

    public function getIsUnregistered() {
        $identity = $this->getRole();

        return $identity == 0 ? true : false;
    }

    public function getIsSuperAdmin() {
        $identity = $this->getRole();

        return $identity == 10 ? true : false;
    }

    public function getIsAdmin() {
        $identity = $this->getRole();

        return $identity == 1 ? true : false;
    }

    public function getIsInstructor() {
        $identity = $this->getRole();

        return $identity == 2 ? true : false;
    }

    public function getIsStudent() {
        $identity = $this->getRole();

        return $identity == 3 ? true : false;
    }

//    public function getIsManager() {
//        $identity = $this->getRole();
//
//        return $identity == 4 ? true : false;
//    }
//
//    public function getIsDeveloper() {
//        $identity = $this->getRole();
//
//        return $identity == 5 ? true : false;
//    }
//    public function getIdentity($autoRenew = true) {
//        if ($this->_identity === false) {
//            if ($this->enableSession && $autoRenew) {
//                $this->_identity = null;
//                $this->renewAuthStatus();
//            } else {
//                return null;
//            }
//        }
//
//        return $this->_identity;
//    }
}
