<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'eRegister',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
//        'authManager' => [
//            'class' => 'yii\rbac\DbManager',
//        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>/<id>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
//        'aliases' => [
//            '@mdm/admin' => '@frontend/extensions/mdm/yii2-admin-2.0.0',
//        // for example: '@mdm/admin' => '@frontend/extensions/mdm/yii2-admin-2.0.0',
//        ],
    ],
];
