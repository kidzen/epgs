<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property Achievement[] $achievements
 * @property Assessment[] $assessments
 * @property Profile $profile
 * @property UserCourseList[] $userCourseLists
 * @property UserCourseList[] $userCourseLists0
 */
class UserList extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $password;
    public $repassword;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['profile_id', 'role', 'status', 'created_by', 'updated_by'], 'integer'],
//            [['username', 'password', 'repassword'], 'required'],
            [['username'], 'required'],
            [['username', 'password', 'repassword'], 'required', 'on' => 'create'],
            [['username'], 'required', 'on' => 'update'],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Both password are not the same.'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return [
            'update' => ['username', 'password', 'repassword', 'role'],
            'create' => ['username', 'password', 'repassword', 'email', 'role']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAchievements() {
        return $this->hasMany(Achievement::className(), ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessments() {
        return $this->hasMany(Assessment::className(), ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function getRank() {
        return $this->hasOne(Rank::className(), ['id' => 'rank_id'])
                        ->via('profile');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourseLists() {
        return $this->hasMany(UserCourseList::className(), ['instructor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourseLists0() {
        return $this->hasMany(UserCourseList::className(), ['profile_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }

}
