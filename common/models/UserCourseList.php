<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "user_course_list".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $course_id
 * @property integer $instructor_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property User $instructor
 * @property CourseList $course
 * @property User $user
 */
class UserCourseList extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_course_list';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['profile_id', 'course_id', 'instructor_id'], 'required'],
            [['profile_id', 'course_id', 'instructor_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['instructor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['instructor_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseList::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'profile_id' => 'User ID',
            'course_id' => 'Course ID',
            'instructor_id' => 'Instructor ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent() {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function getInstructor() {
        return $this->hasOne(Profile::className(), ['id' => 'instructor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse() {
        return $this->hasOne(CourseList::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'profile_id']);
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }

}
