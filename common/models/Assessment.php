<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "assessment".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $profile_id
 * @property integer $user_grade
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property User $user
 * @property QuestionBank $question
 */
class Assessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assessment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comments'], 'string'],
            [['question_id', 'user_course_list_id','profile_grade', 'type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
//            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['profile_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionBank::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'profile_id' => 'User ID',
            'profile_grade' => 'User Grade',
            'user_course_list_id' => 'Trial',
            'comments' => 'Comments',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourseList()
    {
        return $this->hasOne(UserCourseList::className(), ['id' => 'user_course_list_id']);
    }
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id'])
                ->via('userCourseList');
    }
    public function getQuestion()
    {
        return $this->hasOne(QuestionBank::className(), ['id' => 'question_id']);
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }
}
