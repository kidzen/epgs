/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17 : Database - rekamy
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rekamy` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rekamy`;

/*Table structure for table `achievement` */

DROP TABLE IF EXISTS `achievement`;

CREATE TABLE `achievement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `score` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `achievement_profile_id_fk` (`profile_id`),
  KEY `achievement_topic_id_fk` (`topic_id`),
  CONSTRAINT `achievement_topic_id_fk` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`),
  CONSTRAINT `achievement_profile_id_fk` FOREIGN KEY (`profile_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `achievement` */

insert  into `achievement`(`id`,`profile_id`,`topic_id`,`score`,`comment`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,2,1,'1',NULL,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `answer_bank` */

DROP TABLE IF EXISTS `answer_bank`;

CREATE TABLE `answer_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_question_id_fk` (`question_id`),
  CONSTRAINT `answer_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `question_bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `answer_bank` */

insert  into `answer_bank`(`id`,`question_id`,`answer`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,1,'Sc:Ch1:Q1:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(2,1,'Sc:Ch1:Q1:false2',0,0,NULL,NULL,NULL,NULL,0,NULL),(3,1,'Sc:Ch1:Q1:true',1,0,NULL,NULL,NULL,NULL,0,NULL),(4,2,'Sc:Ch1:Q2:false1',0,0,NULL,NULL,NULL,NULL,0,NULL),(5,2,'Sc:Ch1:Q2:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(6,2,'Sc:Ch1:Q2:true',1,0,NULL,NULL,NULL,NULL,0,NULL),(7,3,'Mt:Ch1:Q1:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(8,3,'Mt:Ch1:Q1:false1',0,0,NULL,NULL,NULL,NULL,0,NULL),(11,4,'Mt:Ch1:Q2:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(12,4,'Mt:Ch1:Q2:false1',0,0,NULL,NULL,NULL,NULL,0,NULL),(13,5,'Mt:Ch1:Q3:false1',0,0,NULL,NULL,NULL,NULL,0,NULL),(14,5,'Mt:Ch1:Q3:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(15,6,'Mt:Ch1:Q4:false',0,0,NULL,NULL,NULL,NULL,0,NULL),(16,6,'Mt:Ch1:Q4:false1',0,0,NULL,NULL,NULL,NULL,0,NULL),(19,5,'Mt:Ch1:Q3:true',1,0,NULL,NULL,NULL,NULL,0,NULL),(20,4,'Mt:Ch1:Q2:true',1,0,NULL,NULL,NULL,NULL,0,NULL),(21,3,'Mt:Ch1:Q1:true',1,0,NULL,NULL,NULL,NULL,0,NULL),(22,6,'Mt:Ch1:Q4:true',1,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `assessment` */

DROP TABLE IF EXISTS `assessment`;

CREATE TABLE `assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `user_answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag_answered` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_question_id_fk` (`question_id`),
  KEY `assessment_profile_id_fk` (`profile_id`),
  CONSTRAINT `assessment_profile_id_fk` FOREIGN KEY (`profile_id`) REFERENCES `user` (`id`),
  CONSTRAINT `assessment_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `question_bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `assessment` */

insert  into `assessment`(`id`,`question_id`,`profile_id`,`user_answer`,`flag_answered`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,2,2,'6',1,1,0,NULL,NULL,NULL,NULL,0,NULL),(2,1,2,'1',0,1,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `class_list` */

DROP TABLE IF EXISTS `class_list`;

CREATE TABLE `class_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `class_list` */

/*Table structure for table `course_list` */

DROP TABLE IF EXISTS `course_list`;

CREATE TABLE `course_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `course_list` */

insert  into `course_list`(`id`,`course_name`,`level`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,'Science',NULL,0,NULL,NULL,NULL,NULL,0,NULL),(2,'Mathematics',NULL,0,NULL,NULL,NULL,NULL,0,NULL),(3,'English',NULL,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1468333489),('m130524_201442_init',1470000642),('m160712_193542_profile',1470000642),('m160712_200924_course_list',1470000642),('m160712_201638_user_course_list',1470000643),('m160712_210131_class_list',1470000643),('m160713_052644_topic_under_course',1470000643),('m160713_052707_question_bank',1470000643),('m160714_051642_achievement',1470000644),('m160714_052648_answer',1470000644),('m160716_164844_assessment',1470000645);

/*Table structure for table `profile` */

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poscode` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profile_to_user` (`profile_id`),
  CONSTRAINT `fk_profile_to_user` FOREIGN KEY (`profile_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `profile` */

insert  into `profile`(`id`,`profile_id`,`firstname`,`lastname`,`gender`,`race`,`address`,`state`,`poscode`,`country`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,1,'administrator','administrator lastname','Male','Malay','','',NULL,'',0,NULL,NULL,NULL,NULL,0,NULL),(2,2,'student1name','student1last','Male','Malay','','',NULL,'',0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `question_bank` */

DROP TABLE IF EXISTS `question_bank`;

CREATE TABLE `question_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_bank_topic_fk` (`topic_id`),
  CONSTRAINT `question_bank_topic_fk` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `question_bank` */

insert  into `question_bank`(`id`,`topic_id`,`question`,`hint`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,1,'Sc:Ch1:This is question no 1?','better be good',1,0,NULL,NULL,NULL,NULL,0,NULL),(2,1,'Sc:Ch1:This is question no 2?','',1,0,NULL,NULL,NULL,NULL,0,NULL),(3,3,'Mt:Ch1:Question 1?','',1,0,NULL,NULL,NULL,NULL,0,NULL),(4,3,'Mt:Ch1:Question 2?','',1,0,NULL,NULL,NULL,NULL,0,NULL),(5,3,'Mt:Ch1:Question 3?','',1,0,NULL,NULL,NULL,NULL,0,NULL),(6,3,'Mt:Ch1:Question 4?','',1,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `topic` */

DROP TABLE IF EXISTS `topic`;

CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `topic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topic_course_id_fk` (`course_id`),
  CONSTRAINT `topic_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `course_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `topic` */

insert  into `topic`(`id`,`course_id`,`topic`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,1,'Chapter 1',0,NULL,NULL,NULL,NULL,0,NULL),(2,1,'Chapter 2',0,NULL,NULL,NULL,NULL,0,NULL),(3,2,'Chapter 1',0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT '0',
  `deleted_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`role`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,'admin','Q0UwtX0hBP-0O2wGTi9KvIqn9yVRKmry','$2y$13$xhEUn/kbHlYD1bfMutn9ie2iey/o8VGOvZVNdPtdL2fKZAIgbqHf.',NULL,'admin@gmail.com',1,0,1470061821,0,1470061821,0,0,0),(2,'student1','u_1uXcyupa9-rmEdrdTxVJSVm3hjwReh','$2y$13$rBNZXh39TmvPUbQCS4vAXuUoYAykBTfSM1U7j0RsT7wbaBnEb04my',NULL,'student1@gmail.com',3,0,1470062360,0,1470062360,0,0,0),(3,'teacher1','nzh9G5_Y_CGzRpCo-uADRtEfbW7jJytD','$2y$13$JD4DBu8dq8wKTW6x7VJzYe7DxS3PrH311IuppYew52V4YiD1h2eXu',NULL,'teacher1@gmail.com',2,0,1470062491,0,1470062491,0,0,0);

/*Table structure for table `user_course_list` */

DROP TABLE IF EXISTS `user_course_list`;

CREATE TABLE `user_course_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_course_list_course_list` (`course_id`),
  KEY `fk_user_course_list_user` (`profile_id`),
  CONSTRAINT `fk_user_course_list_user` FOREIGN KEY (`profile_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_user_course_list_course_list` FOREIGN KEY (`course_id`) REFERENCES `course_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_course_list` */

insert  into `user_course_list`(`id`,`profile_id`,`course_id`,`role`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,3,1,NULL,0,NULL,NULL,NULL,NULL,0,NULL),(2,3,2,NULL,0,NULL,NULL,NULL,NULL,0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
