<?php

namespace frontend\components\constant;

class Enum 
{
    public function Transaction($type){
        if($type == 1){
            return 'Check In';
        } else if ($type == 2){
            return 'Check Out';
        } else {
            return 'Undefined';
        }
    }
    
    public function Status($type){
        if($type == 10){
            return 'Active';
        } else if ($type <= 0) {
            return 'Deleted';
        } else {
            return 'Undefined';
        }
    }
    
    public function Approval($type){
        if($type == 0){
            return 'New';
        } else if ($type == 1){
            return 'Pending';
        } else if ($type == 2){
            return 'Approved';
        } else if ($type == 8){
            return 'Rejected';
        } else if ($type == 9){
            return 'Disposed';
        } else {
            return 'Undefined';
        }
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
transaction type : 
    1 = Check In
    2 = Check Out
        
        
Approval Type :
    0 = New
    1 = Pending
    2 = Approved
    8 = Rejected
    9 = Disposed
        
        
Status/Deleted :
    0 = Active
    1 = Deleted
 * 
 */
 */