<?php
/* @var $this yii\web\View */
//var_dump($studentList);die();
//var_dump($courseList);die();
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-users"></i>

                <h3 class="box-title">Student List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ol>
                    <?php foreach ($courseList as $j => $course) { ?>
                        <li><?= $course['course_name'] ?></li>
                        <ul>
                            <?php foreach ($studentList as $i => $student) { ?>
                                <li><?= kartik\helpers\Html::a($student['firstname'], ['grade/test', 'id' => $student['id'], 'testId' => $course['id']]) ?>
                                <?php } ?>
                        </ul>
                        </li>
                    <?php } ?>
                </ol>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>