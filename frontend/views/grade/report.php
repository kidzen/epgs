<?php
/* @var $this yii\web\View */
//var_dump($studentList);die();
//var_dump($courseList);die();
?>
<div class="row">
    <?php foreach ($courseList as $j => $course) { ?>

        <div class="col-md-10">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-bullseye"></i>

                    <h3 class="box-title"><?= $course['course_name'] ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ol>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Consectetur adipiscing elit</li>
                        <li>Integer molestie lorem at massa</li>
                        <li>Facilisis in pretium nisl aliquet</li>
                        <li>Nulla volutpat aliquam velit
                            <ol>
                                <li>Phasellus iaculis neque</li>
                                <li>Purus sodales ultricies</li>
                                <li>Vestibulum laoreet porttitor sem</li>
                                <li>Ac tristique libero volutpat at</li>
                            </ol>
                        </li>
                        <li>Faucibus porta lacus fringilla vel</li>
                        <li>Aenean sit amet erat nunc</li>
                        <li>Eget porttitor lorem</li>
                    </ol>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    <?php } ?>
</div>