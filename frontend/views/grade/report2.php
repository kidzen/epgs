<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Inflector::humanize($student->firstname) . ' Assessments Report';
$this->params['breadcrumbs'][] = $this->title;

$title = empty($this->title) ? Yii::t('app', 'Report') : $this->title . ' Report';
?>
<div class="assessment-index">

    <?php Pjax::begin(); ?>            <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create assessment'),])//. ' ' .
            //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
            //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'exportConfig' => [
            'pdf' => ['title' => $this->title],
            'csv' => '{csv}',
            'xls' => '{xls}',
        ],
        'export' => [
            'fontAwesome' => true,
            'target' => '_self',
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //[
            //      'attribute'=>'id',
            //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
            //],
            [
                'attribute' => 'question.question',
                'format' => 'raw',
                'value' => function($model) {
                    if ($model->question->subquestion) {
                        return \yii\helpers\Html::tag('span', $model->question->question . " :  " . $model->question->subquestion);
//                        return \yii\helpers\Html::tag('span', $model->question->question . " : <br> " . \yii\helpers\Html::tag('span', $model->question->subquestion, ['class' => 'pull-right']));
//                        return ;
                    } else {
                        return \yii\helpers\Html::encode($model->question->question);
                    }
                },
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'profile.firstname',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'profile_grade',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'comments',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
//            [
//                'attribute' => 'type',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'status',
//                'class' => 'kartik\grid\BooleanColumn',
//                'trueIcon' => '<span class="label label-success">ACTIVE</span>',
//                'falseIcon' => '<span class="label label-danger">DELETED</span>',
//                'falseLabel' => 'DELETED',
//                'trueLabel' => 'ACTIVE',
//            ],
            //[
            //      'attribute'=>'created_at',
            //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
            //],
            //[
            //      'attribute'=>'created_by',
            //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
            //],
            //[
            //      'attribute'=>'updated_at',
            //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
            //],
            //[
            //      'attribute'=>'updated_by',
            //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
            //],
            [
                'class' => 'kartik\grid\ActionColumn',
                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isSuperAdmin,
                'template' => '{view}{update}{delete}{recover}',
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'buttons' => [
                    'recover' => function ($url, $model) {
                        if ($model->status === 0) {
                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip']);
                        }
                    },
                            'delete' => function ($url, $model) {
                        if ($model->status === 1) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                        'data-method' => 'post']);
                        }
                    },
                        ],
                    ],
//                    [
//                        'header' => 'Permanent Delete',
//                        'class' => 'kartik\grid\ActionColumn',
//                        'template' => '{delete-permanent}',
//                        'buttons' => [
//                            'delete-permanent' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
//                                            'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip'
//                                ]);
//                            }
//                                ],
//                                'visible' => Yii::$app->user->isAdmin,
//                            ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?></div>
