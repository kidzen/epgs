<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Assessment */

$this->title = 'Update Assessment: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Assessments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="assessment-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
