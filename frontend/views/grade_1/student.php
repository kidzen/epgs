<?php
/* @var $this yii\web\View */
//var_dump($studentList);die();
//var_dump($courseList);die();
?>
<div class="row">
    <div class="col-md-4">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-users"></i>

                <h3 class="box-title">Student List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ol>
                    <?php foreach ($courseList as $j => $course) { ?>
                        <li><?= $course['course_name'] ?></li>
                        <ul>
                            <?php foreach ($studentList as $i => $student) { ?>
                            <li><?= kartik\helpers\Html::a($student['firstname'], ['grade/test','id'=>$student['id'],'testId'=>$course['id']]) ?>
                                <?php } ?>
                        </ul>
                        </li>
                    <?php } ?>
                </ol>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
    <div class="col-md-4">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-bullseye"></i>

                <h3 class="box-title">Ordered Lists</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ol>
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>Consectetur adipiscing elit</li>
                    <li>Integer molestie lorem at massa</li>
                    <li>Facilisis in pretium nisl aliquet</li>
                    <li>Nulla volutpat aliquam velit
                        <ol>
                            <li>Phasellus iaculis neque</li>
                            <li>Purus sodales ultricies</li>
                            <li>Vestibulum laoreet porttitor sem</li>
                            <li>Ac tristique libero volutpat at</li>
                        </ol>
                    </li>
                    <li>Faucibus porta lacus fringilla vel</li>
                    <li>Aenean sit amet erat nunc</li>
                    <li>Eget porttitor lorem</li>
                </ol>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
    <div class="col-md-4">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-text-width"></i>

                <h3 class="box-title">Unstyled List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="list-unstyled">
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>Consectetur adipiscing elit</li>
                    <li>Integer molestie lorem at massa</li>
                    <li>Facilisis in pretium nisl aliquet</li>
                    <li>Nulla volutpat aliquam velit
                        <ul>
                            <li>Phasellus iaculis neque</li>
                            <li>Purus sodales ultricies</li>
                            <li>Vestibulum laoreet porttitor sem</li>
                            <li>Ac tristique libero volutpat at</li>
                        </ul>
                    </li>
                    <li>Faucibus porta lacus fringilla vel</li>
                    <li>Aenean sit amet erat nunc</li>
                    <li>Eget porttitor lorem</li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>