<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
//var_dump($student);die();
//var_dump($course);die();
//var_dump($questions);die();
?>
<div class="row">
    <div class="col-md-10">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-bullseye"></i>

                <h3 class="box-title"><?= $course->course_name ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!--                <ol>
                <?php foreach ($questions as $question) { ?>
                                                                                                            <li><?= $question->question ?></li>
                                                                                                            <ol>
                                                                                                                <li>Phasellus iaculis neque</li>
                                                                                                            </ol>
                                                                                                            </li>
                <?php } ?>
                                </ol>-->


            </div>

            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>STUDENT</th>
                                <th><?= $student->firstname ?></th>
                                <th>SORTIE NO</th>
                                <th><?= $student->id ?></th>
                                <th>UNIT</th>
                                <th><?= $student->unit ?></th>
                            </tr>
                            <tr>
                                <th>INSTRUCTOR</th>
                                <th><?= Yii::$app->user->profileName ?></th>
                                <th>DURATION</th>
                                <th></th>
                                <th>DATE</th>
                                <th><?= date('d-m-Y') ?></th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-bordered-black">
                        <thead>
                            <tr>
                                <th rowspan="2">NO</th>
                                <th rowspan="2">EXERCISES</th>
                                <th rowspan="2">GRADE</th>
                                <th rowspan="2">COMMENT</th>
                            </tr>
<!--                            <tr>
                                <th>NO</th>
                                <th>EXERCISES</th>
                                <th>GRADE</th>
                                <th>COMMENT</th>
                            </tr>-->

                        </thead>
                        <tbody>
                            <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                            <?php foreach ($questions as $i => $question) { ?>
                                <tr>
                                    <td><?= $qno = $i + 1 ?></td>
                                    <td><?= $question->question ?></td>
                                    <?php echo $form->field($model[$question->id], "[$question->id]question_id")->hiddenInput(['value' => $question->id])->label(false) ?>
                                    <?= $form->field($model[$question->id], "[$question->id]profile_id")->hiddenInput(['value' => $student->id])->label(false) ?>
                                    <!--<td><?= $form->field($model[$question->id], "[$question->id]profile_grade")->radio() ?></td>-->
                                    <td><?= $form->field($model[$question->id], "[$question->id]profile_grade")->inline()->radioList(['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5], ['separator' => " | "])->label(false) ?></td>
                                    <!--<td><?= $form->field($model[$question->id], "[{$i}]profile_grade")->radioList(['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5], ['separator' => " | "])->inline()->label(false) ?></td>-->
<!--                                    <td><?=
                                        $form->field($model[$question->id], "[{$i}]profile_grade")->radioList(['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5], [
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="radio-inline">' . Html::radio($name, $checked, ['value' => $value]) . $label . '</label>';
                                            }
                                                ])->inline()->label(false)
                                                ?></td>-->

                                            <!--                                    <td><?=
                                    $form->field($model[1], 'profile_grade')->listBox(
                                            array('1' => '1', 2 => '2', 3 => 3, 4 => 4, 5 => 5), array('prompt' => 'Select')
                                    );
                                                ?></td>-->



                                            <td><?= kartik\helpers\Html::textarea($model, null, ['class' => 'full-width full-height']) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                        <div>
                            <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
                            <?= Html::submitButton($model[$question->id]->isNewRecord ? 'Create' : 'Update', ['class' => $model[$question->id]->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>