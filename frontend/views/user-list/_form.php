<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'profile_id')->textInput() ?>
                        </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
                        </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <?php if (Yii::$app->user->isAdmin) { ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'role')->dropDownList([1=>'Administrator', 2=>'Instructor', 3=>'Student']) ?>
                </div>
            <?php } ?>

        </div>
        <div>
            <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
