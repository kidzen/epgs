<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model2, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model2, 'password')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model2, 'repassword')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model2, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <?php if (Yii::$app->user->isAdmin) { ?>
                <div class="col-md-4">
                    <?= $form->field($model2, 'role')->dropDownList([1 => 'Administrator', 2 => 'Instructor', 3 => 'Student'], ['prompt' => '--select role--']) ?>
                </div>
            <?php } ?>
            <div class="col-md-4">
                <?= $form->field($model, 'rank_id')->dropDownList($rankArray,['prompt'=>'-- select rank --']) ?>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="panel-title">User Details</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female'], ['prompt' => '-- select --']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'race')->dropDownList(['Malay' => 'Malay', 'Others' => 'Others'], ['prompt' => '-- select --']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'poscode')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
