<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CourseList */

$this->title = 'Create Course List';
$this->params['breadcrumbs'][] = ['label' => 'Course Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-list-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
