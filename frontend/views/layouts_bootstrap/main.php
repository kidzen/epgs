<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'e-Register',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top delay',
                ],
            ]);
//            $menuItems[] = [
//                    'class' => 'navbar-inverse navbar-fixed-top animated bounceInDown',
//                ];
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
//                ['label' => 'About', 'url' => ['/site/about'],],
//                ['label' => 'Contact', 'url' => ['/site/contact'],],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//            } else if (Yii::$app->user->can('seeProfile')) {
//                $menuItems[] = ['label' => 'Profile', 'url' => ['/profile/index']];
//                $menuItems[] = ['label' => 'Permission', 'url' => ['/permission/view',['id'=>  Yii::$app->user->id]]];
//            } else if (Yii::$app->user->can('admin')) {
//                $menuItems[] = ['label' => 'User', 'url' => ['/profile/index']];
//                $menuItems[] = ['label' => 'Permission', 'url' => ['/permission/index']];
            } else {
                $menuItems[] = ['label' => 'Profile', 'url' => ['/profile/index']];
                $menuItems[] = ['label' => 'Create Rule', 'url' => ['/permission/create-rule']];
                $menuItems[] = ['label' => 'Create Permission', 'url' => ['/permission/create-permission']];
                $menuItems[] = ['label' => 'Assign Permission', 'url' => ['/permission/permission-assignment']];
                $menuItems[] = ['label' => 'Permission', 'url' => ['/permission/index']];
                $menuItems[] = '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link']
                        )
                        . Html::endForm()
                        . '</li>';
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
