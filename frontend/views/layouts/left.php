<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/default-avatar.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->name ? Yii::$app->user->name : 'Guest'; ?></p>

                <a href="#"><i class="fa fa-circle text-<?= Yii::$app->user->status == 'Online' ? 'success' : 'danger' ?>"></i> <?= Yii::$app->user->status ?></a>
            </div>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                        <span class="input-group-btn">
                            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>-->
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu delay'],
                    'items' => [
                        ['label' => 'e-PGS', 'options' => ['class' => 'header']],
//                        if user is offline
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest, 'options' => ['class' => 'delay-child'],],
//                        ['label' => 'Take An Assesment', 'url' => ['user-assessment/my-index'], 'visible' => Yii::$app->user->isStudent, 'options' => ['class' => 'delay-child'],],
//                        if user is student
                        [
                            'label' => 'My Dashboard',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'visible' => Yii::$app->user->isStudent ,
                            'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'My Profile', 'url' => ['profile/view', 'id' => Yii::$app->user->id],],
                                ['label' => 'My Course Achievement', 'url' => ['user-course-list/index2', 'id' => Yii::$app->user->id],],
                            ],
                        ],
//                        [
//                            'label' => 'Course Management',
//                            'icon' => 'fa fa-user',
//                            'url' => '#',
//                            'visible' => Yii::$app->user->isAdmin ||  Yii::$app->user->isInstructor,
//                            'options' => ['class' => 'delay-child'],
//                            'items' => [
//                                ['label' => 'Course Request', 'icon' => 'fa fa-file-code-o', 'url' => ['user-course-list/create'],],
////                                ['label' => 'Course List', 'icon' => 'fa fa-dashboard', 'url' => ['course-list/index'],],
//                            ],
//                        ],
//                        [
//                            'label' => 'Assesment',
////                            'icon' => 'fa fa-user',
//                            'url' => '#',
//                            'visible' => Yii::$app->user->isStudent,
//                            'options' => ['class' => 'delay-child'],
//                            'items' => [
//                                ['label' => 'Topic List','url' => ['topic/my-index', 'id' => Yii::$app->user->id],],
//                                ['label' => 'My Course', 'url' => ['user-course-list/my-index', 'id' => Yii::$app->user->id],],
//                                ['label' => 'My Schedule', 'url' => ['class/my-index', 'id' => Yii::$app->user->id],],
//                                ['label' => 'My Schedule', 'url' => ['achievement/myindex', 'id' => Yii::$app->user->id],],
//                            ],
//                        ],
//                        if user is instructor
                        [
                            'label' => 'My Dashboard',
                            'icon' => 'fa fa-users',
                            'url' => '#',
                            'visible' => Yii::$app->user->isInstructor , 'options' => ['class' => 'delay-child'],
                            'items' => [
//                                ['label' => 'Grading', 'icon' => 'fa fa-file-code-o', 'url' => ['user-course-list/index'],],
                                ['label' => 'My Profile', 'icon' => 'fa fa-file-code-o', 'url' => ['user-list/view', 'id' => Yii::$app->user->id],],
//                                ['label' => 'My Schedule', 'icon' => 'fa fa-file-code-o', 'url' => ['class/my-index', 'id' => Yii::$app->user->id],],
                                ['label' => 'List of Student', 'icon' => 'fa fa-file-code-o', 'url' => ['profile/student'],],
                                ['label' => 'List of Course', 'icon' => 'fa fa-file-code-o', 'url' => ['course-list/index'],],
                            ],
                        ],
                        ['label' => 'Grade Student', 'icon' => 'fa fa-dashboard', 'url' => ['grade/student'],'visible' => Yii::$app->user->isInstructor || Yii::$app->user->isAdmin,],
                        ['label' => 'Grade Report', 'icon' => 'fa fa-dashboard', 'url' => ['user-course-list/index'],'visible' => Yii::$app->user->isInstructor || Yii::$app->user->isAdmin,],
//                        [
//                            'label' => 'Course Management',
//                            'icon' => 'fa fa-users',
//                            'url' => '#',
//                            'visible' => Yii::$app->user->isInstructor, 'options' => ['class' => 'delay-child'],
//                            'items' => [
//                                ['label' => 'Course Request', 'icon' => 'fa fa-file-code-o', 'url' => ['user-course-list/create'],],
//                                ['label' => 'Course List', 'icon' => 'fa fa-dashboard', 'url' => ['user-course-list/index', 'id' => Yii::$app->user->id],],
//                            ],
//                        ],
//                        [
//                            'label' => 'Assesment Manager',
//                            'icon' => 'fa fa-users',
//                            'url' => '#',
//                            'visible' => Yii::$app->user->isInstructor, 'options' => ['class' => 'delay-child'],
//                            'items' => [
//                                ['label' => 'Grade Student', 'icon' => 'fa fa-file-code-o', 'url' => ['grading/student'],],
////                                ['label' => 'Create Topic', 'icon' => 'fa fa-file-code-o', 'url' => ['topic/create'],],
////                                ['label' => 'Create Question', 'icon' => 'fa fa-file-code-o', 'url' => ['question-bank/create'],],
////                                ['label' => 'Create Answer', 'icon' => 'fa fa-file-code-o', 'url' => ['answer-bank/create'],],
////                                ['label' => 'Question Bank', 'icon' => 'fa fa-file-code-o', 'url' => ['answer-bank/index'],],
//                            ],
//                        ],
//                        [
//                            'label' => 'Class Management',
//                            'icon' => 'fa fa-users',
//                            'url' => '#',
//                            'visible' => Yii::$app->user->isInstructor, 'options' => ['class' => 'delay-child'],
//                            'items' => [
//                                ['label' => 'My Class', 'icon' => 'fa fa-file-code-o', 'url' => ['class/view','id'=>Yii::$app->user->id],],
//                                ['label' => 'Class List', 'icon' => 'fa fa-dashboard', 'url' => ['class/index'],],
//                            ],
//                        ],
//                        if user is admin
                        [
                            'label' => 'Administrator',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'visible' => Yii::$app->user->isAdmin, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'Student List', 'icon' => 'fa fa-dashboard', 'url' => ['profile/student'],],
                                ['label' => 'Instructor List', 'icon' => 'fa fa-dashboard', 'url' => ['profile/instructor'],],
                                ['label' => 'User Management', 'icon' => 'fa fa-dashboard', 'url' => ['user-list/index'],],
//                                ['label' => 'My Profile', 'icon' => 'fa fa-file-code-o', 'url' => ['user-list/view', 'id' => Yii::$app->user->id],],
                            ],
                        ],
                        [
                            'label' => 'System Management',
                            'icon' => 'fa fa-dashboard',
                            'url' => '#',
                            'visible' => Yii::$app->user->isAdmin, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'Course Management', 'icon' => 'fa fa-dashboard', 'url' => ['course-list/index'],],
                                ['label' => 'Question Management', 'icon' => 'fa fa-dashboard', 'url' => ['question-bank/index'],],
                                ['label' => 'Rank Management', 'icon' => 'fa fa-dashboard', 'url' => ['rank/index'],],
//                                        ['label' => 'Demo Database', 'icon' => 'fa fa-file-code-o', 'url' => ['migration/index'],],
                            ],
                        ],
//                        if user is developer
                        [
                            'label' => 'Dev Tool',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'visible' => Yii::$app->user->isAdmin, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
//                                [
//                                    'label' => 'Level One',
//                                    'icon' => 'fa fa-circle-o',
//                                    'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                        [
//                                            'label' => 'Level Two',
//                                            'icon' => 'fa fa-circle-o',
//                                            'url' => '#',
//                                            'items' => [
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                            ],
//                                        ],
//                                    ],
//                                ],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
