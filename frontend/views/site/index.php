<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>150</h3>

                    <p>New Assessment</p>
                </div>
                <div class="icon">
                    <i class="fa fa-bolt"></i>
                </div>
                <a href="#" class="small-box-footer">Start assessment <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>53<sup style="font-size: 20px">%</sup></h3>

                    <p>Last Result</p>
                </div>
                <div class="icon">
                    <i class="fa fa-clipboard"></i>
                </div>
                <a href="#" class="small-box-footer">Detail report <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>44</h3>

                    <p>Course Registered</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user-plus"></i>
                </div>
                <a href="#" class="small-box-footer">More course <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>65</h3>

                    <p>Settings </p>
                </div>
                <div class="icon">
                    <i class="fa fa-gears"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="jumbotron">
        <h1>OPGAS</h1>

        <p class="lead">Online Pilot Grade Assessment System</p>
        <?php
        if (!Yii::$app->user->id) {
            echo html::a('Start Now!', ['site/login'], ['class' => 'btn btn-lg btn-success animated bounceInLeft']);
        } else if (Yii::$app->user->isStudent) {
            echo html::a('Take Assessment Now!', ['assessment/index'], ['class' => 'btn btn-lg btn-success animated bounceInLeft']);
//        } else if (Yii::$app->user->isTeacher) {
//            echo html::a('Hai', ['contact'], ['class' => 'btn btn-lg btn-success animated bounceInLeft']);
        } else {
            echo html::a('Looking For Help?', ['contact'], ['class' => 'btn btn-lg btn-success animated bounceInLeft']);
        }
        ?>
        <?php // echo html::a('Hai', ['contact'], ['class' => 'btn btn-lg btn-success animated bounceInLeft']);   ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Student Module</h2>

                <p><b>Student </b>can review grade and result of their Pilot Grade Assessment. They can also print result</p>

                <!--<p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>-->
            </div>
            <div class="col-lg-4">
                <h2>Instructor Module</h2>

                <p><b>Instructor </b>can grade student for their respective pilot grading assessment review student result.</p>

                <!--<p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>-->
            </div>
            <div class="col-lg-4">
                <h2>Administrator Module</h2>

                <p><b>Administrator </b>can register student/instructor and setup role. Admin can also create assessment. Admin have all the priveledges to the system.</p>

                <!--<p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>-->
            </div>
        </div>

    </div>
</div>
