<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\CourseList;
use common\models\CourseListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;

class GradeController extends \yii\web\Controller {

    public function actionStudent() {
        $studentList = \common\models\Profile::find()->joinWith('user')->where(['user.role' => 3])->asArray()->all();
        $courseList = \common\models\CourseList::find()->asArray()->all();

        return $this->render('student', [
                    'studentList' => $studentList,
                    'courseList' => $courseList,
        ]);
    }

    public function actionReport($studentId,$testId) {
        $student = \common\models\Profile::findOne($studentId);
        $searchModel = new \common\models\AssessmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_course_list.profile_id' => $studentId]);
        $dataProvider->query->andWhere(['user_course_list.id' => $testId]);
        return $this->render('report2', [
                    'student' => $student,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest($id, $testId) {

        $count = count(\Yii::$app->request->post('Assessment', []));

        $student = \common\models\Profile::findOne($id);
        $instructor = \common\models\Profile::findOne(\Yii::$app->user->id);
        $course = \common\models\CourseList::findOne($testId);
        $questions = \common\models\QuestionBank::findAll(['course_id' => $testId]);
//        var_dump($questions);die();
        foreach ($questions as $question) {
            $models[$question->id] = new \common\models\Assessment();
        }
        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            $userCourseList = new \common\models\UserCourseList();
            $userCourseList->profile_id = $id;
            $userCourseList->course_id = $testId;
            $userCourseList->instructor_id = \Yii::$app->user->id;
            $userCourseList->save();
            
            foreach ($models as $model) {
                $model->user_course_list_id = $userCourseList->id;
                $flag = $model->save(false);
            }
            

            if ($flag) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-check-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
//                die('success');
//            return $this->redirect(['report',['id'=>$student->id]]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
//            die();
            return $this->redirect(['report', 'studentId' => $student->id,'testId'=>$testId]);
//            return $this->redirect(['report'], ['id' => $student->id]);
        } else {
            return $this->render('test', [
                        'model' => $models,
                        'student' => $student,
                        'instructor' => $instructor,
                        'course' => $course,
                        'questions' => $questions,
            ]);
        }
    }

    public function actionTest3($id, $testId) {

        $student = \common\models\Profile::findOne($id);
        $instructor = \common\models\Profile::findOne(\Yii::$app->user->id);
        $course = \common\models\CourseList::findOne($testId);
        $questions = \common\models\QuestionBank::findAll(['course_id' => $testId]);
//        foreach ($questions as $question) {
//            $model[$question->id] = new \common\models\Assessment;
//        }
        $model = [new \common\models\Assessment];
        if ($model->load(\Yii::$app->request->post())) {
            var_dump($model);
            die();
//            if ($post = \Yii::$app->request->post()) {
//                var_dump($post);die();
////                var_dump($post[2]);
//                $questionNo = 1;
//                foreach ($post as $i=>$value) {
//                    var_dump($value[$i]);die();
//                    $model = new \common\models\Assessment;
//                    $model->question_id = $questionNo;
//                    $model->profile_id = $id;
//                    $model->profile_grade = $value[$i];
//                    var_dump($x +1);
//                }
//            }
//            var_dump($model);
            die();
        }
        return $this->render('test2', [
                    'model' => $model,
                    'student' => $student,
                    'instructor' => $instructor,
                    'course' => $course,
                    'questions' => $questions,
        ]);
    }

    public function actionTest2($id, $testId) {
//        $this->enableCsrfValidation = false;
        $student = \common\models\Profile::findOne($id);
        $instructor = \common\models\Profile::findOne(\Yii::$app->user->id);
        $course = \common\models\CourseList::findOne($testId);
        $questions = \common\models\QuestionBank::findAll(['course_id' => $testId]);
        foreach ($questions as $question) {
            $model[$question->id] = new \common\models\Assessment;
        }
//        var_dump($model[1]->attributes);die();
//        die();
        if (\Yii::$app->request->post()) {

            var_dump(\Yii::$app->request->post());
            die();
//            if ($post = \Yii::$app->request->post()) {
////                var_dump($post[1]);
////                var_dump($post[2]);
////                $questionNo = 1;
////                foreach ($post as $i=>$value) {
////                    var_dump($value[$i]);die();
////                    $model = new \common\models\Assessment;
////                    $model->question_id = $questionNo;
////                    $model->profile_id = $id;
////                    $model->profile_grade = $value[$i];
////                    var_dump($x +1);
////                }
//            }
//            var_dump($model);
            die();
        }
        return $this->render('test2', [
                    'model' => $model,
                    'student' => $student,
                    'instructor' => $instructor,
                    'course' => $course,
                    'questions' => $questions,
        ]);
    }

    public function actionReport2($id) {
//        var_dump($id);die();
        $studentList = \common\models\Profile::find()->joinWith('user')->where(['user.role' => 3])->asArray()->all();
        $courseList = \common\models\UserCourseList::find()->where(['profile_id' => $id])->asArray()->all();


        return $this->render('report', [
                    'studentList' => $studentList,
                    'courseList' => $courseList,
        ]);
    }

    public function actionIndex() {
        $searchModel = new \common\models\UserListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

}
